@extends('app')

@section('content')
<section class="container">
	<div class="row">
		<div class="col col-lg-12">

            <article class="pageTitle">
				<h1>Lorem ipsum dolor sit amet onset consequat massa quis enim.</h1>
			</article>

			@foreach($contents as $content)
                @if($content->type == "Left-title")
                    <article class="col col-lg-4 pageSubTitle">
                        <h1>{!! $content->content1 !!}</h1>
                        <div class="col col-lg-6 titleLine"></div>
                    </article>
                @endif
                @if($content->type == "Right-content")
                    <article class="col col-lg-7 col-lg-offset-1 textBlock">
                        <p>{!! $content->content1 !!}</p>
                    </article>
                @endif
                @if($content->type == "image")
                    <article class="col col-lg-7 col-lg-offset-5 textBlock">
                        <img src="<?php echo env('IMGURL')?>{!! $content->content1 !!}" alt="placeholderImage" />
                    </article>
                @endif
                @if($content->type == "seperator")
                    <div class="clear"></div>
                    <div class="margin-bottom"> </div>
                @endif
            @endforeach
            <div class="clear"></div>

		</div>
	</div>
</section>
@endsection
