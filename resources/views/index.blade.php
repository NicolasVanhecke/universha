@extends('appHome')

@section('content')
<!-- Header home -->
@foreach($randomImg as $img)
<section id="homeHeaderWrapper">
    <article class="homeHeaderWrapperImage" style="background-image: url('{{env('IMGURL').$img->image}}')">
        <div class="skewBlue"></div>
        <div class="skewWhite"></div>
        <div class="container">
            <div class="row">
                <img src="{{ asset('images/logo.png') }}"/>
                <div class="col col-lg-3 col-md-3 col-sm-5 col-xs-4 welcometext">
                    @foreach($snippets as $snippet)
                        @if($snippet->name == "home-header-title")
                            <h3>{!! $snippet->content !!}</h3>
                        @endif
                    @endforeach
                    @foreach($snippets as $snippet)
                        @if($snippet->name == "home-header-text")
                            <p>{!! $snippet->content !!}</p>
                        @endif
                    @endforeach
                </div>
            </div>
        </div>
        <div class="homeHeaderUnderImage"></div>
    </article>
</section>
@endforeach


<section class="container">
	<div class="row">
		<div class="col col-lg-12">
			<article class="pageTitleHome">
				<h1>Universha biedt een forum voor leden en sympathisanten met interesse voor de haven en transport.</h1>
			</article>

			<article class="col col-lg-7 col-md-7 col-sm-12">
                @foreach($snippets as $snippet)
                    @if($snippet->name == "home-welcome-text")
                        <p>{!!$snippet->content!!}</p>
                    @endif
                @endforeach
                <div class="linkButton">
					<i class="fa fa-arrow-right"></i>
					<p>Lees verder</p>
				</div>
			</article>

			<article class="col col-lg-4 col-lg-offset-1 col-md-4 col-md-offset-1 col-sm-12 volgende">
				<h4>Volgende event</h4>
				<div class="lightgreyBoxVolgend">
					<div class="date">3 februari 2015</div>
					<div class="short">De rol van de 'goederecontroleur' binnen de internationale en lokale handel</div>
					<div class="name">Speakers:</div>
					<div class="description">Spanoghe en Johan Pype van SGS</div>
					<div class="name">Info:</div>
					<div class="description">Walking dinner, ...</div>
						
					<div class="linkButton">
						<i class="fa fa-arrow-right"></i>
						<p>Lees verder</p>
					</div>
				</div>				
			</article>
			
			<article class="col col-lg-12">
				<h4>Vorige events</h4>
				<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.
Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec.</p>
				<article class="lightgreyBoxVorige col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div class="date col-lg-2 col-md-2 col-sm-2 col-xs-2">12/2014</div>
					<div class="title col-lg-9 col-md-9 col-sm-9 col-xs-8">De haven van Zeebrugge. Toespraak door Emmanuel Van Damme, Sales Director Port of Zeebrugge</div>
					<div class="clickVerderButton"><div class="arrow"><img class="arrow" src="{{ asset('images/arrow.png') }}" /></div></div>
				</article>
				<article class="lightgreyBoxVorige col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div class="date col-lg-2 col-md-2 col-sm-2 col-xs-2">12/2014</div>
					<div class="title col-lg-9 col-md-9 col-sm-9 col-xs-8">De haven van Zeebrugge. Toespraak door Emmanuel Van Damme, Sales Director Port of Zeebrugge</div>
                    <div class="clickVerderButton"><div class="arrow"><img class="arrow" src="{{ asset('images/arrow.png') }}" /></div></div>
				</article>				
				<article class="lightgreyBoxVorige col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div class="date col-lg-2 col-md-2 col-sm-2 col-xs-2">12/2014</div>
					<div class="title col-lg-9 col-md-9 col-sm-9 col-xs-8">De haven van Zeebrugge. Toespraak door Emmanuel Van Damme, Sales Director Port of Zeebrugge</div>
                    <div class="clickVerderButton"><div class="arrow"><img class="arrow" src="{{ asset('images/arrow.png') }}" /></div></div>
				</article>				

				<article class="bekijkAlle">
					<i class="fa fa-arrow-right"><span>Bekijk alle events</span></i>
				</article>								
			</article>
			
			<article>
				<h4>Universha in beeld</h4>
                <div id="gallery">
                    @foreach($galleryImgs as $img)
                        <a class="galleryItem col-lg-3 col-lg-offset-0 col-md-4 col-md-offset-1 col-sm-4 col-sm-offset-0 col-xs-8 col-xs-offset-2" href="<?php
                        echo env('IMGURL')?>{!!
                        $img->image !!}"
                           data-lightbox="gallery">
                            <div class="overlay">{!! $img->caption !!}</div>
                            <img class="galleryImage" src="<?php echo env('IMGURL')?>{!! $img->image !!}" data-lightbox="universha" >
                        </a>
                    @endforeach
                </div>

                {{--<div id="gallery">--}}
	                {{--<a class="galleryItem col-lg-3 col-md-4 col-sm-6 col-xs-12" href="http://lorempixel.com/760/300" data-lightbox="gallery">--}}
	                    {{--<div class="overlay">￼Aenean commodo ligula eget dolor</div>--}}
	                    {{--<img class="galleryImage" src="http://lorempixel.com/760/300" />--}}
	                {{--</a>--}}
	                {{--<a class="galleryItem col-lg-3 col-md-4 col-sm-6 col-xs-12" href="http://lorempixel.com/800/400" data-lightbox="gallery">--}}
	                    {{--<div class="overlay">￼Aenean commodo ligula eget dolor</div>--}}
	                    {{--<img class="galleryImage" src="http://lorempixel.com/800/400" />--}}
					{{--</a>--}}
				{{--</div>--}}
			</article>
			
		</div>
	</div>
</section>

@endsection
