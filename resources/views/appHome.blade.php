<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Universha</title>

	<!-- Laravel 5 CSS -->
	<link href="{{ asset('/css/app.css') }}" rel="stylesheet">
		
	<!-- Own CSS -->
	<link href="{{ asset('/css/main.css') }}" rel="stylesheet">
	<link href="{{ asset('/css/lightbox.css') }}" rel="stylesheet">

	<!-- Fonts -->
	<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
	<link href='//fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'>
	<link href='//fonts.googleapis.com/css?family=PT+Sans+Narrow:400,700' rel='stylesheet' type='text/css'>

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>
<body>
	<!-- Header -->
	<!-- ------ -->
	<nav class="navbar navbar-default">
	  <div class="container">
	    <!-- Brand and toggle get grouped for better mobile display -->
	    <div class="navbar-header">
	      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
	        <span class="sr-only">Toggle navigation</span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	      </button>
	    </div>
	
	    <!-- Collect the nav links, forms, and other content for toggling -->
	    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
	      <ul class="nav navbar-nav navbar-right">
              @foreach($menu as $label)
                  <li><a href="{!! URL::to($label['label']) !!}"><?php echo $label['label'] ?></a></li>
              @endforeach
	      </ul>
	    </div><!-- /.navbar-collapse -->
	  </div><!-- /.container-fluid -->
	</nav>


	<!-- Load page-specific content -->
	<!-- -------------------------- -->
	@yield('content')
	
	<!-- Footer -->
	<!-- ------ -->
	
	<section id="homeFooterWrapper">
		<article class="homeFooterWrapperImage">
			<div class="blueOverlay"></div>
			<div class="container">
				<div class="row">
					<div class="col col-lg-6 col-lg-offset-3 schrijfIn">
						<h3>￼Maak u lid van Universha en blijf op de hoogte van al onze activiteiten</h3>
						<div class="linkButton">
							<i class="fa fa-arrow-right"></i>
							<p>Schrijf u in</p>
						</div>
					</div>			
				</div>			
			</div>
		</article>
	</section>

	<section id="postFooterWrapper">
		<article class="postFooter">
			<div class="container">
				<div class="row">
					<div class="col col-lg-6 col-md-4 col-sm-12" style="margin-bottom: 50px;">
						<div class="col col-lg-3 col-md-0 col-sm-0 logo">
							<img src="{{ asset('images/logo.png') }}" alt="logo"/>
						</div>
						<div class="col col-lg-9 col-md-12 col-sm-12">
                            @foreach($snippets as $snippet)
                                @if($snippet->name == "footer-title")
                                    <h2>{!!$snippet->content!!}</h2>
                                @endif
                            @endforeach
                            @foreach($snippets as $snippet)
                                @if($snippet->name == "footer-text")
                                    <p>{!!$snippet->content!!}</p>
                                @endif
                            @endforeach
							<div class="social">
								<div class="bol fb"><img src="{{ asset('images/fb.png') }}" alt="fb"/></div>
								<div class="bol tw"><img src="{{ asset('images/tw.png') }}" alt="tw"/></div>
								<div class="bol li"><img src="{{ asset('images/li.png') }}" alt="li"/></div>
							</div>					
						</div>					
					</div>			
					
					<div class="col col-lg-6 col-md-8 col-sm-12">
						<div class="col col-lg-6 col-md-6 col-sm-6 col-xs-6 xxs">
							<h2>Navigeer op onze site</h2>
							<ul class="sitemap">
                                @foreach($menu as $label)
                                    <li><a href="{!! URL::to($label['label']) !!}"><?php echo $label['label'] ?></a></li>
                                @endforeach
							</ul>
						</div>
						<div class="col col-lg-6 col-md-6 col-sm-6 col-xs-6 xxs">
							<div class="linkButton">
								<i class="fa fa-arrow-right"></i>
								<p>info@universha.be</p>
							</div>
							<div class="linkButton">
								<i class="fa fa-arrow-right"></i>
								<p>T 03 456 78 90</p>
							</div>
						</div>					
					</div>							
				</div>			
			</div>
		</article>
	</section>
	
	<!-- Scripts -->
	<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.1/js/bootstrap.min.js"></script>
	<script src="{{ asset('/js/lightbox.min.js') }}"></script>
	<script src="{{ asset('/js/main.js') }}"></script>
</body>
</html>
