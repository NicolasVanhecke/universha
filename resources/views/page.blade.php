@extends('app')

@section('content')

<!-- Header all pages -->
@foreach($randomImg as $img)
<section id="headerWrapper">
    <article class="headerWrapperImage" style="background-image: url('{{env('IMGURL').$img->image}}')">
        <div class="skewWhite"></div>
        <div class="container">
            <div class="row">
                <img src="{{ asset('images/logo.png') }}"/>
            </div>
        </div>
        <div class="headerUnderImage"></div>
    </article>
</section>
@endforeach


<section class="container">
	<div class="row">
		<div class="col col-lg-12">

            <article class="pageTitle">
				<h1>Lorem ipsum dolor sit amet onset consequat massa quis enim.</h1>
			</article>

			@foreach($contents as $content)
                @if($content->type == "Left-title")
                    <article class="col col-lg-4 pageSubTitle">
                        <h1>{!! $content->content1 !!}</h1>
                        <div class="col col-lg-6 titleLine"></div>
                    </article>
                @endif
                @if($content->type == "Right-content")
                    <article class="col col-lg-7 col-lg-offset-1 textBlock">
                        <p>{!! $content->content1 !!}</p>
                    </article>
                @endif
                @if($content->type == "Right-content-no-title")
                    <article class="col col-lg-7 col-lg-offset-5 textBlock">
                        <p>{!! $content->content1 !!}</p>
                    </article>
                @endif
                @if($content->type == "image")
                    <article class="col col-lg-7 col-lg-offset-5 textBlock">
                        <img src="<?php echo env('IMGURL')?>{!! $content->content1 !!}" alt="placeholderImage" />
                    </article>
                @endif
                @if($content->type == "seperator")
                    <div class="clear"></div>
                    <div class="margin-bottom"> </div>
                @endif
            @endforeach
            <div class="clear"></div>

        </div>
	</div>
</section>

@if($pageStub == "inschrijven")
    <article class="row">
        <article class="inschrijvenStappenWrapper container">
            <div class="stap activeStap col-lg-4 col-md-4 col-sm-4 col-xs-4">stap 1</div>
            <div class="stap col-lg-4 col-md-4 col-sm-4 col-xs-4">stap 2</div>
            <div class="stap col-lg-4 col-md-4 col-sm-4 col-xs-4">stap 3</div>
        </article>
    </article> <!-- end inschrijvingen wrapper -->

    <section id="inschrijvenWrapper" class="">
            <div class="container">
                <div class="row">
                    <div class="col col-lg-12">
                        <!-- form wrapper -->
                        <article class="formWrapper col col-lg-12">

                            {!! Form::open(array('action' => 'PageController@sendForm')) !!}
                            <div class="" id="inschrijvenForm">
                                <div class="formValidation">
                                    @foreach ($errors->all() as $error)
                                        <p class="formValidationError">{{ $error }}</p>
                                    @endforeach
                                </div>
                                <article>
                                    {!! Form::text('voornaam', null, array('name' => 'voornaam', 'placeholder' => 'Voornaam'))  !!}
                                </article>
                                <article>
                                    {!! Form::text('naam', null, array('name' => 'naam', 'placeholder' => 'Naam'))  !!}
                                </article>
                                <article>
                                    {!! Form::text('adres', null, array('name' => 'adres', 'placeholder' => 'Adres'))  !!}
                                </article>
                                <article>
                                    {!! Form::text('telefoon', null, array('name' => 'telefoon', 'placeholder' => 'Telefoon'))  !!}
                                </article>
                                <article>
                                    {!! Form::text('bedrijf', null, array('name' => 'bedrijf', 'placeholder' => 'Bedrijf'))  !!}
                                </article>
                                <article>
                                    {!! Form::text('BTWnr', null, array('name' => 'BTWnr', 'placeholder' => 'BTW nr'))  !!}
                                </article>
                                <article>
                                    {!! Form::text('aantal', null, array('name' => 'aantal', 'placeholder' => 'Aantal personen'))  !!}
                                </article>
                                {!! Form::close() !!}

                                <button class="linkButton" type="submit">
                                    <i class="fa fa-arrow-right"></i>Verder
                                </button>
                            </div>
                        </article> <!-- end form wrapper -->
                    </div>
                </div>
            </div>
        </section>
@endif

@endsection
