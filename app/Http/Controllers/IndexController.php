<?php namespace App\Http\Controllers;

use App\Content;
use App\Menu;
use App\Page;
use App\Slider;
use App\Snippet;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;

class IndexController extends Controller {

    public function index()
    {

        $pageData = Page::where('stub', '=', 'home')->firstOrFail();
        $pageId = $pageData['id'];

        $menu = Menu::where('visible', '=', '1')
            ->orderBy('rank', 'asc')->get();

        // get random image for header background
        $randomImg = Slider::where('slider', '=', 'home')
            ->take(1)
            ->orderByRaw("RAND()")->get();

        $contents = Content::where('pageId', '=', $pageId)
            ->orderBy('rank', 'ASC')->get();

        // get all images from slider for in gallery
        $galleryImgs = Slider::where('slider', '=', 'over-ons')
            ->orderBy('rank', 'asc')->get();

        $snippets = Snippet::all();

        return view('index')
            ->with('menu', $menu)
            ->with('randomImg', $randomImg)
            ->with('contents', $contents)
            ->with('galleryImgs', $galleryImgs)
            ->with('snippets', $snippets);

    }

    public function create()
    {
        return 'create';
    }

    public function store()
    {
        return 'store';
    }

    public function show()
    {
        return 'show';
    }

    public function edit()
    {
        return 'edit';
    }

    public function destroy()
    {
        return 'destroy';
    }
}
