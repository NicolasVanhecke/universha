<?php namespace App\Http\Controllers;

use App\Content;
use App\Page;
use App\Slider;
use App\Snippet;
use App\Menu;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;

use Symfony\Component\HttpFoundation\Request;


class PageController extends Controller {


    // get stub from url for
    public function page($stub)
    {

        $pageData = Page::where('stub', '=', $stub)->firstOrFail();
        $pageId = $pageData['id'];
        $pageStub = $pageData['stub'];

        $menu = Menu::where('visible', '=', '1')
            ->orderBy('rank', 'asc')->get();

        // get random image for header background
        $randomImg = Slider::where('slider', '=', 'other')
            ->take(1)
            ->orderByRaw("RAND()")->get();

        $contents = Content::where('pageId', '=', $pageId)
            ->orderBy('rank', 'ASC')->get();

        $snippets = Snippet::all();

        return view('page')
            ->with('pageStub', $pageStub)
            ->with('menu', $menu)
            ->with('randomImg', $randomImg)
            ->with('contents', $contents)
            ->with('snippets', $snippets);
    }



    // link to inschrijven page in footer
    public function inschrijven($stub)
    {
        $pageData = Page::where('stub', '=', $stub)->firstOrFail();
        $pageId = $pageData['id'];

        $menu = Menu::where('visible', '=', '1')
            ->orderBy('rank', 'asc')->get();

        $snippets = Snippet::all();
        $contents = Content::where('pageId', '=', $pageId)
            ->orderBy('rank', 'ASC')->get();

        return view('inschrijven')
            ->with('contents', $contents)
            ->with('snippets', $snippets)
            ->with('menu', $menu);
    }



    public function sendForm()
    {
        $data = Input::all();

        //Validation rules
        $rules = array (
            'voornaam'  => 'required',
            'naam'      => 'required',
            'adres'     => 'required',
            'telefoon'  => 'required',
            'bedrijf'   => 'required',
            'BTWnr'     => 'required',
            'aantal'    => 'required'
        );

        $validator  = Validator::make ($data, $rules);

        //If Validation passes, send submission form.
        if ($validator -> passes()){

            Mail::send('emails.sendForm', array('date' => 'today'), function($form)
            {
                $form->to('vanhnico@gmail.com')->subject('Universha inschrijving');
            });

            Session::flash('message', "Uw inschrijving is succesvol naar ons doorgestuurd.");
            return Redirect::back();

        }else{

            //return contact form with errors
            return Redirect::to('/inschrijven')->withErrors($validator);
        }
    }   
}
