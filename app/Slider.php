<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Slider extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'sliders';

    /**
     * The table id used by the model.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = array();

    public function page() {
        return $this->belongsTo('Page');
    }

}
