/**
 * Created by Nicolas on 07/05/15.
 */

$(document).ready(function() {
    console.log("ready");
    sizeOverlayGallery();
    sizeOverlayFooter();
    //sizeVorigeEventsLink();
    positionArrows();
    verticalAlignment();
});

// Also load size calculating functions on window resize
$(window).resize(function() {
    sizeOverlayGallery();
    sizeOverlayFooter();
});

function sizeOverlayGallery(){

    maxHeight = 0;
    maxWdith = 0;

    $("#gallery .galleryImage").each(function(){

        if ($(this).height() > maxHeight)
        {
            maxHeight = $(this).height() - 60;
        }
        if ($(this).width() > maxWdith)
        {
            maxWdith = $(this).width() - 60;
        }
    });

    $("#gallery .overlay").height(maxHeight);
    $("#gallery .overlay").width(maxWdith);
}

function sizeOverlayFooter(){

    maxHeight = 0;

    $("#homeFooterWrapper .homeFooterWrapperImage").each(function(){
        if ($(this).height() > maxHeight)
        {
            maxHeight = $(this).height();
        }
    });
    $("#homeFooterWrapper .blueOverlay").height(maxHeight);
    
    $("#footerWrapper .footerWrapperImage").each(function(){
        if ($(this).height() > maxHeight)
        {
            maxHeight = $(this).height();
        }     
    });
    $("#footerWrapper .blueOverlay").height(maxHeight);
}

function positionArrows(){

    var styles = {'padding-top': 'auto'};
    $(".clickVerderButton img").css(styles);

    paddingTop = 0;

    $(".clickVerderButton").each(function(){
        if ($(this).height() > paddingTop)
        {
            paddingTop = ($(this).height() - 20) / 2;
        }
    });

    styles = {'padding-top':paddingTop};
    $(".clickVerderButton img").css(styles);

}

function verticalAlignment(){

    var styles = {'line-height': 'auto' };
    $(".lightgreyBoxVorige .date").css(styles);
    $(".lightgreyBoxVorige .title").css(styles);

    lineHeight = 0;

    $(".lightgreyBoxVorige").each(function(){
        if ($(this).height() > lineHeight)
        {
            lineHeight = $(this).height();
        }
    });

    styles = {'line-height': (lineHeight+'px') };
    $(".lightgreyBoxVorige .date").css(styles);
    $(".lightgreyBoxVorige .title").css(styles);

}

